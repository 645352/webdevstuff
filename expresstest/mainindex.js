const express = require('express');
const handlebars = require('express-handlebars');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const passportSocket = require('passport.socketio');
const async = require('async');
const passport = require('passport');
const SteamStrategy = require('passport-steam').Strategy;
const path = require('path');
const mongoose = require('mongoose');
const http = require('http');
const socket = require('socket.io');
const MongoStore = require('connect-mongo')(session);
const config = require('./config.json');
const app = express();
const server = http.Server(app);
const io = socket(server);
const hbs = handlebars.create();
var steamidd;
var currentcredits;
var lobbies = [];
var currentroom;
var pressedbutton;
const sessionStore = new MongoStore({
  mongooseConnection: mongoose.connection
});

mongoose.connect('mongodb://127.0.0.1:27017/guide');
passport.serializeUser((user, done) => {
  done(null, user._json);
  firsttimescsript(user.id);
  steamidd = user.id;
  console.log("steamidd" + steamidd);


});

passport.deserializeUser((obj, done) => {
    done(null, obj);
});
var userschema = mongoose.Schema({
  userid: Number, credit: Number
});
//model for moongose stuff and quarries
var steamuser = mongoose.model('steamuser', userschema);

passport.use(
  new SteamStrategy(
    {
      returnURL: 'http://localhost:3037/auth/steam/return',
      realm: 'http://localhost:3037/',
      apiKey: config.apiKey
    },
    (identifier, profile, done) => {
      return done(null, profile);
    }
  )
);

io.use(
  passportSocket.authorize({
    cookieParser: cookieParser,
    key: 'U_SESSION',
    secret: config.secretString,
    store: sessionStore
  })
);

io.on('connection', function (socket) {
  
  var localroll;
  var opproll;

  var oppready;
  var meready;

  socket.on('disconnect', function(){
    inlobby = false;
    if(currentroom != undefined){
       lobbies[currentroom] = lobbies[currentroom] -1; 
       console.log("left room "+currentroom+" player in room "+ lobbies[currentroom]);
    }
    currentroom = undefined;
    
  });
  console.log('a user connected');
  
  socket.emit('console','local');
  socket.on('reset')
  socket.on('enemyroll', function (oppenentroll) {
        opproll = oppenentroll;
        console.log(' enemyroll '+opproll + oppenentroll);
    

  });
  
  if(meready ==true){


  }
  else{
    socket.on('localready',function(){

      socket.emit('console','ready');
      var x = Math.floor((Math.random() * 100) + 1);
      localroll =x;
      console.log('roll '+ localroll);
      socket.to(currentroom).emit('enemyrollc', localroll);
      if(oppready==true){
        
        avbcredit(steamidd);
        socket.emit('console', 'credits before '+currentcredits);
        
        compareroll(localroll,opproll);
        avbcredit(steamidd);
        socket.emit('console', 'credits after '+currentcredits);
  
        socket.emit('console', 'local roll ' +localroll+' opp roll'+opproll);
        socket.to(currentroom).emit('console', 'local roll ' +opproll+' opp roll'+localroll);
        
        console.log('roll '+ roll);
      }
      meready = true;
      socket.to(currentroom).emit('opntreadyc');
      console.log('local ready called ready= '+meready);
      
  
    });
    
  }
 
  socket.on('opntready',function(){
    socket.to(currentroom).emit('enemyrollc', localroll);
    socket.emit('console','opponent ready');
    if(meready==true){
      avbcredit(steamidd);
      socket.emit('console', 'credits before '+currentcredits);
      
      compareroll(localroll,opproll);
      avbcredit(steamidd);
      socket.emit('console', 'credits after '+currentcredits);
      console.log('roll '+ localroll+' '+ opproll);

      socket.emit('console', 'local roll ' +localroll+' opp roll'+opproll);
      socket.to(currentroom).emit('console', 'local roll ' +opproll+' opp roll'+localroll);
      
    }
    

  });
  socket.on('a', function () {
        
    if(pressedbutton==99999999999999999999){
        
    }
    else{
        findroom(socket);
        socket.emit('ready'); 
        
        socket.to(currentroom).emit('console', "notlocal");
    }
        
    
       

  });
});


app.engine('hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(
  session({
    secret: config.secretString,
    name: 'U_SESSION',
    resave: true,
    saveUninitialized: true,
    store: sessionStore
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('public'));
app.use(cookieParser());
                                                                          
app.get('/', (req, res) => {
  res.render('main', {
    user: req.user,credits:currentcredits
  });
});


app.get('/coinflip', (req, res) => {
  res.render('coinflip', {
    credits:currentcredits
  });
});


app.get(
  /^\/auth\/steam(\/return)?$/,
  passport.authenticate('steam', { failureRedirect: '/' }),
  (req, res) => {
    res.redirect('/');
  }
);

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});


function changecredit(add,userid) {
  steamuser.findOne({ 'userid': userid }, 'userid credit', function (err, person) {
      if (person == null) { console.log("user not found"); }
      else {

          person.credit = person.credit + add;
          person.save(function (err, updatedcredit) {
              if (err) return handleError(err);
              console.log(err);
              console.log(person);
          });

      }
      console.log(err);
  });
}
function avbcredit(userid) {
  steamuser.findOne({ 'userid': userid }, 'userid credit', function (err, person) {
      if (person == null) { console.log("user not found"); }
      else {

          
          currentcredits = person.credit;
          console.log("currentcredits"+currentcredits);
      }
      console.log(err);
  });
}
function firsttimescsript(steamid) {
  steamuser.findOne({ 'userid': steamid }, 'userid credit', function (err, person) {
      if (person == null) {
          var user = new steamuser({ userid: steamid, credit: 1 });
          user.save((err, user) => {

          });


          console.log("creat new user");
      }
      else {

          console.log("user exist");

          console.log(person);


      }
  });

}
server.listen(3037, () => {
  console.log('listening');
});
function findroom(socket){
    var roomindex = 0;
    var roomlenght = lobbies.length;
    while(roomindex<=roomlenght){
        if(lobbies[roomindex]==undefined){
            lobbies[roomindex]=0;
        }
        if(lobbies[roomindex]==0){
            pressedbutton = true;
            lobbies[roomindex]=1;
            socket.join(roomindex);
            socket.emit('console', 'joined'+roomindex);
            socket.emit('console', 'with '+lobbies[roomindex]);
            currentroom = roomindex;
            socket.emit('onready');
            break;
        }

        else if(lobbies[roomindex]==1){
            lobbies[roomindex]=2;
            socket.join(roomindex);
            socket.emit('console', 'joined w'+roomindex);
            pressedbutton = true
            socket.emit('console', 'with '+lobbies[roomindex]);
            currentroom = roomindex;
            socket.emit('onready');
            break;
        }
        roomindex++;
    }
    socket.emit('console', 'current'+currentroom);
    socket.emit('console', 'current'+steamidd);
    io.sockets.in(currentroom).emit('console', steamidd +' joined');
}
function compareroll(roll,oppenentroll){
    if(roll<oppenentroll){
        changecredit(-100,steamidd);

    }
    if(roll>oppenentroll){
        changecredit(+100,steamidd);
    }
    if(roll==oppenentroll){
       changecredit(-100,steamidd);
    }

}