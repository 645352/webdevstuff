const express = require('express');
const handlebars = require('express-handlebars');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const passportSocket = require('passport.socketio');
const async = require('async');
const passport = require('passport');
const SteamStrategy = require('passport-steam').Strategy;
const path = require('path');
const mongoose = require('mongoose');
const http = require('http');
const socket = require('socket.io');
const MongoStore = require('connect-mongo')(session);
const config = require('./config.json');


const app = express();
const server = http.Server(app);
const io = socket(server);
const hbs = handlebars.create();
var steamidd;
var currentcredits;
const sessionStore = new MongoStore({
  mongooseConnection: mongoose.connection
});

mongoose.connect('mongodb://127.0.0.1:27017/guide');


passport.serializeUser((user, done) => {
  done(null, user._json);
  firsttimescsript(user.id);
  steamidd = user.id;
  console.log("steamidd" + steamidd);


});

passport.deserializeUser((obj, done) => {
    done(null, obj);
});
var userschema = mongoose.Schema({
  userid: Number, credit: Number
});
//model for moongose stuff and quarries
var steamuser = mongoose.model('steamuser', userschema);

passport.use(
  new SteamStrategy(
    {
      returnURL: 'http://localhost:3037/auth/steam/return',
      realm: 'http://localhost:3037/',
      apiKey: config.apiKey
    },
    (identifier, profile, done) => {
      return done(null, profile);
    }
  )
);

io.use(
  passportSocket.authorize({
    cookieParser: cookieParser,
    key: 'U_SESSION',
    secret: config.secretString,
    store: sessionStore
  })
);

io.on('connection', function (socket) {
  console.log('a user connected');
  
  socket.on('a', function () {
      avbcredit(steamidd);
      var x = Math.floor((Math.random() * 2) + 1);
      console.log(x);
      if(currentcredits>0){
              if (x == 2) {
                changecredit(10,steamidd);
                avbcredit(steamidd);
          socket.emit('welcome', currentcredits);
      }
      else {
        changecredit(-10,steamidd);
        avbcredit(steamidd);
          socket.emit('welcome', currentcredits);
      }
      }
      else{

      }

      
      
       

  });
});


app.engine('hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(
  session({
    secret: config.secretString,
    name: 'U_SESSION',
    resave: true,
    saveUninitialized: true,
    store: sessionStore
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('public'));
app.use(cookieParser());

app.get('/', (req, res) => {
  res.render('main', {
    user: req.user
  });
});


app.get('/coinflip', (req, res) => {
  res.render('coinflip', {
    credits:'hello world'
  });
});


app.get(
  /^\/auth\/steam(\/return)?$/,
  passport.authenticate('steam', { failureRedirect: '/' }),
  (req, res) => {
    res.redirect('/');
  }
);

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});


function changecredit(add,userid) {
  steamuser.findOne({ 'userid': userid }, 'userid credit', function (err, person) {
      if (person == null) { console.log("user not found"); }
      else {

          person.credit = person.credit + add;
          person.save(function (err, updatedcredit) {
              if (err) return handleError(err);
              console.log(err);
              console.log(person);
          });

      }
      console.log(err);
  });
}
function avbcredit(userid) {
  steamuser.findOne({ 'userid': userid }, 'userid credit', function (err, person) {
      if (person == null) { console.log("user not found"); }
      else {

          
          currentcredits = person.credit;
          console.log("currentcredits"+currentcredits);
      }
      console.log(err);
  });
}
function firsttimescsript(steamid) {
  steamuser.findOne({ 'userid': steamid }, 'userid credit', function (err, person) {
      if (person == null) {
          var user = new steamuser({ userid: steamid, credit: 1 });
          user.save((err, user) => {

          });


          console.log("creat new user");
      }
      else {

          console.log("user exist");

          console.log(person);


      }
  });

}
server.listen(3037, () => {
  console.log('listening');
});