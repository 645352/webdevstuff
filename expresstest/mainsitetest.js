 const express = require('express');
const handlebars = require('express-handlebars');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const passportSocket = require('passport.socketio');
const async = require('async');
const passport = require('passport');
const SteamStrategy = require('passport-steam').Strategy;
const path = require('path');
const mongoose = require('mongoose');
const http = require('http');
const socket = require('socket.io');
const MongoStore = require('connect-mongo')(session);
const config = require('./config.json');
var inlobby;
var lobbies = [];
const app = express();
const server = http.Server(app);
const io = socket(server);
const hbs = handlebars.create();
var steamidd;
var currentcredits;
var currentroom;
var playersinroom = [[],[]];
const sessionStore = new MongoStore({
  mongooseConnection: mongoose.connection
});

mongoose.connect('mongodb://127.0.0.1:27017/guide');


passport.serializeUser((user, done) => {
  done(null, user._json);
  firsttimescsript(user.id);
  steamidd = user.id;
  console.log("steamidd" + steamidd);


});

passport.deserializeUser((obj, done) => {
    done(null, obj);
});
var userschema = mongoose.Schema({
  userid: Number, credit: Number
});
//model for moongose stuff and quarries
var steamuser = mongoose.model('steamuser', userschema);

passport.use(
  new SteamStrategy(
    {
      returnURL: 'http://localhost:3037/auth/steam/return',
      realm: 'http://localhost:3037/',
      apiKey: '6DD1DF0BA107E03AFD8A215AD3E0D4BC'
    },
    (identifier, profile, done) => {
      return done(null, profile);
    }
  )
);

io.use(
  passportSocket.authorize({
    cookieParser: cookieParser,
    key: 'U_SESSION',
    secret: config.secretString,
    store: sessionStore
  })
);

io.on('connection', function (socket) {
  socket.emit('console', lobbies[0]);
  socket.emit('console',lobbies[1]);
    socket.on('disconnect', function(){

        removesteamidfromarray();
        inlobby = false;
        if(currentroom != undefined){
           lobbies[currentroom] = lobbies[currentroom] -1; 
           console.log("left room "+currentroom+" player in room "+ lobbies[currentroom]);
        }
        currentroom = undefined;
        
      });
  console.log('a user connected');
  socket.emit('console', 'connected');
  socket.on('a', function () {
    socket.emit('console', 'current'+currentroom);
    socket.emit('console', 'current'+steamidd);

    console.log("lobbies"+lobbies);
    socket.emit('console', 'pressed');
      if(inlobby == true){
          return;
      }
    console.log('steamidd'+steamidd);
    
    
    console.log(lobbies);
    var lobbielenght = lobbies.length;
    console.log("lobbie lenght" + lobbielenght);
    var a = 0;
    //loop that start at 0 and looks for an open room with 1 player
    while( a <= lobbielenght){
        socket.emit('console', 'loop');
        if(lobbies[a]==undefined){
            lobbies[a]=0;
            console.log("lobby "+a+" is undefined");
        }
        if(lobbies[a]==null){
            lobbies[a]=0;
            console.log("lobby "+a+" is null");
        }
        if(lobbies[a]==11){
            socket.join(a);
            
            //joinlobby
            
            io.sockets.in(a).emit('welcome', 1);
            lobbies[a] = lobbies[a]+1;
            currentroom = a;
            inlobby = true;
            addsteamidtoarray();
            console.log("joining lobby "+a+" with "+ lobbies[a]);
            break;
        }
        else if(lobbies[a]==0){
            socket.join(a);
            
            io.sockets.in(a).emit('console', steamidd +'joined');
            lobbies[a] = lobbies[a]+1;
            currentroom = a;
            inlobby = true;
            addsteamidtoarray();
            console.log("joining lobby "+a+" with "+ lobbies[a]);
            break;
        }
     
        a++;

    }

});
});


app.engine('hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(
  session({
    secret: config.secretString,
    name: 'U_SESSION',
    resave: true,
    saveUninitialized: true,
    store: sessionStore
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('public'));
app.use(cookieParser());

app.get('/', (req, res) => {
  res.render('main', {
    user: req.user
  });
});


app.get('/coinflip', (req, res) => {
  res.render('coinflip', {
    credits:'hello world'
  });
});


app.get(
  /^\/auth\/steam(\/return)?$/,
  passport.authenticate('steam', { failureRedirect: '/' }),
  (req, res) => {
    res.redirect('/');
  }
);

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});


function changecredit(add,userid) {
  steamuser.findOne({ 'userid': userid }, 'userid credit', function (err, person) {
      if (person == null) { console.log("user not found"); }
      else {

          person.credit = person.credit + add;
          person.save(function (err, updatedcredit) {
              if (err) return handleError(err);
              console.log(err);
              console.log(person);
          });

      }
      console.log(err);
  });
}
function avbcredit(userid) {
  steamuser.findOne({ 'userid': userid }, 'userid credit', function (err, person) {
      if (person == null) { console.log("user not found"); }
      else {

          
          currentcredits = person.credit;
          console.log("currentcredits"+currentcredits);
      }
      console.log(err);
  });
}
function firsttimescsript(steamid) {
  steamuser.findOne({ 'userid': steamid }, 'userid credit', function (err, person) {
      if (person == null) {
          var user = new steamuser({ userid: steamid, credit: 1 });
          user.save((err, user) => {

          });


          console.log("creat new user");
      }
      else {

          console.log("user exist");

          console.log(person);


      }
  });

}
server.listen(3037);
function addsteamidtoarray(){ 
    
    if(playersinroom[currentroom][0]==undefined){
        playersinroom[currentroom][0] = 0;

    }
    if(playersinroom[currentroom][0]==0){
        playersinroom[currentroom][0] = steamidd;

    }
    else if(playersinroom[currentroom][1]==0){
        playersinroom[currentroom][1] = steamidd;
           

    }
}
function removesteamidfromarray(){
/*
    if(playersinroom[currentroom][0]==steamidd){
        console.log("remove id from "+playersinroom[currentroom][0]);
        playersinroom[currentroom][0] = 0;

    }
    else if(playersinroom[currentroom][1]==steamidd){
        console.log("remove id from "+playersinroom[currentroom][1]);
        playersinroom[currentroom][1] = 0;
           

    }
*/
}